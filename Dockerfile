FROM debian:buster

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install maven vim -y

RUN useradd -m cicd_user

WORKDIR /home/cicd_user/

RUN mkdir ./gitlab-cicd/

COPY . ./gitlab-cicd/

RUN chown 1000:1000 -R ./gitlab-cicd/

USER 1000

WORKDIR /home/cicd_user/gitlab-cicd/

CMD mvn spring-boot:run
# CMD ["./mvnw", "spring-boot:run"]
